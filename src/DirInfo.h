#pragma once

#include <QtCore/QObject>
#include <QtCore/QMap>

/**
 * Класс для сканирования каталога и выдачи информации о нем
 */
class DirInfo : public QObject
{
Q_OBJECT

public:
    struct Info
    {
        quint32 filesCount_ {0};
        quint32 dirsCount_ {0};
        quint64 size_ {0};
        using CountAndSize = QPair<quint32, quint64>;
        std::map<QString, CountAndSize> typesSize_ {};
    };

public:
    /**
     * path - путь к каталогу, который необходимо сканировать
     * parent - родительский объект
     */
    explicit DirInfo (const QString &path, QObject *parent);

    virtual ~DirInfo ();

    /**
     * Возвращает путь, по которому производится сканирование
     */
    QString path () const;

    /**
     * Возвращает текущую информацию о каталоге
     * @return Info
     */
    Info info () const;

    /**
     * Проверка на завершенность сканирования
     * @return true, если процесс завершен
     */
    bool isFinished () const;

Q_SIGNALS:

    /**
     * Сигнал об обновлении информации
     */
    void infoChanged ();

    /**
     * Сигнал о завершении сканирования
     */
    void finished ();

private:
    Q_DISABLE_COPY (DirInfo)

private:
    class Impl;

    Impl *impl_;
};

