#include <QtCore/QDebug>
#include <QtCore/QPointer>
#include <QtCore/QDirIterator>
#include <QtCore/QElapsedTimer>
#include <QtCore/QMutex>

#include <QtConcurrent/QtConcurrentRun>

#include "DirInfo.h"

constexpr quint16 msecToEmitChanged = 500;

class DirInfo::Impl : public QObject
{
Q_OBJECT

public:
    Impl (const QString &path, QObject *parent)
        : QObject (parent)
        , info_ {}
        , path_ (path)
        , stop_ (false)
        , finished_ (false)
        , mutex_ {}
        , future_ (QtConcurrent::run (DirInfo::Impl::scanDir, std::ref (*this)))
    {
    }

    virtual ~Impl ()
    {
        stop_ = true;
        future_.waitForFinished ();
    }

    static void scanDir (DirInfo::Impl &impl)
    {
        QDirIterator it (impl.path_, QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        DirInfo::Info &info = impl.info_;
        QElapsedTimer timer {};
        timer.start ();
        while (!impl.stop_ && it.hasNext ()) {
            it.next ();
            const QFileInfo &fileInfo = it.fileInfo ();
            if (fileInfo.isDir ()) {
                if (fileInfo.absolutePath () == impl.path_) {
                    ++info.dirsCount_;
                }
            } else {
                ++info.filesCount_;
                info.size_ += fileInfo.size ();
                const QString type = fileInfo.suffix ();
                if (!type.isEmpty ()) {
                    if (info.typesSize_.find (type) == info.typesSize_.end ()) {
                        auto &countAndSize = info.typesSize_[type];
                        ++countAndSize.first;
                        countAndSize.second += fileInfo.size ();
                    } else {
                        QMutexLocker locker (&impl.mutex_);
                        info.typesSize_.emplace (
                            type, DirInfo::Info::CountAndSize {1, static_cast<const quint64 &>(fileInfo.size ())}
                        );
                    }
                }
            }
            if (timer.elapsed () > msecToEmitChanged) {
                emit impl.infoChanged ();
                timer.restart ();
            }
        }
        impl.finished_ = true;
        if (!impl.stop_) {
            emit impl.infoChanged ();
            emit impl.finished ();
        }
    }

    Info info () const
    {
        QMutexLocker locker (&mutex_);
        return info_;
    }

    QString path () const
    {
        return path_;
    }

    bool isFinished () const
    {
        return finished_;
    }

Q_SIGNALS:

    void infoChanged ();

    void finished ();

private:
    Q_DISABLE_COPY (Impl)

private:
    Info info_;
    const QString path_;
    bool stop_;
    bool finished_;
    mutable QMutex mutex_;
    QFuture<void> future_;
};

DirInfo::DirInfo (const QString &path, QObject *parent)
    : QObject (parent)
    , impl_ (new Impl (path, this))
{
    connect (impl_, &DirInfo::Impl::infoChanged, this, &DirInfo::infoChanged, Qt::QueuedConnection);
    connect (impl_, &DirInfo::Impl::finished, this, &DirInfo::finished, Qt::QueuedConnection);
}

DirInfo::~DirInfo ()
{

}

DirInfo::Info DirInfo::info () const
{
    return impl_->info ();
}

QString DirInfo::path () const
{
    return impl_->path ();
}

bool DirInfo::isFinished () const
{
    return impl_->isFinished ();
}

#include <DirInfo.moc>
