#include <QtCore/QStorageInfo>
#include <QtCore/QDebug>

#include <QtWidgets/QFileSystemModel>

#include "DirInfo.h"

#include "ui_MainWindow.h"
#include "MainWindow.h"

MainWindow::MainWindow ()
    : QMainWindow (nullptr)
    , ui_ (std::make_unique<Ui_MainWindow> ())
    , dirTreeModel_ (new QFileSystemModel (this))
    , dirInfoList_ {}
    , currentDirInfo_ (nullptr)
    , currentDirInfoConnection_ {}
{
    ui_->setupUi (this);

    dirTreeModel_->setRootPath ("/");
    dirTreeModel_->setFilter (QDir::Dirs | QDir::NoDotAndDotDot);
    ui_->dirTree_->setModel (dirTreeModel_);

    for (const QStorageInfo &info : QStorageInfo::mountedVolumes ()) {
        ui_->driveSelector_->addItem (info.displayName (), info.rootPath ());
    }
    connect (ui_->driveSelector_, static_cast<void (QComboBox::*) (int)> (&QComboBox::activated),
             this, &MainWindow::onDriveSelect
    );
    connect (ui_->dirTree_, &QTreeView::clicked, this, &MainWindow::onFolderSelect);
}

MainWindow::~MainWindow ()
{
}

void MainWindow::onDriveSelect (int index)
{
    ui_->dirTree_->setRootIndex (dirTreeModel_->index (ui_->driveSelector_->itemData (index).toString ()));
}

void MainWindow::onFolderSelect (const QModelIndex &index)
{
    const QString &path = dirTreeModel_->filePath (index);
    const auto it = std::find_if (dirInfoList_.begin (), dirInfoList_.end (),
                                  [path] (DirInfo *dirInfo) {
                                      return dirInfo->path () == path;
                                  }
    );
    DirInfo *dirInfo = nullptr;
    if (it == dirInfoList_.end ()) {
        dirInfo = new DirInfo (path, this);
        connect (dirInfo, &DirInfo::finished, this, &MainWindow::updateCurrentProcessesInfo);
        dirInfoList_.push_back (dirInfo);
    } else {
        dirInfo = *it;
    }

    changeCurrentDirInfo (dirInfo);
    updateInfoPanel (dirInfo);
    updateCurrentProcessesInfo ();
}

void MainWindow::changeCurrentDirInfo (DirInfo *dirInfo)
{
    if (currentDirInfo_) {
        QObject::disconnect (currentDirInfoConnection_);
    }
    currentDirInfo_ = dirInfo;
    currentDirInfoConnection_ = connect (currentDirInfo_, &DirInfo::infoChanged, [this] () { updateInfoPanel (); });
}

void MainWindow::updateInfoPanel (DirInfo *dirInfo)
{
    if (!dirInfo) {
        dirInfo = currentDirInfo_;
    }

    const DirInfo::Info &info = dirInfo->info ();
    ui_->filesCountLabel_->setText (QString::number (info.filesCount_));
    ui_->dirsCountLabel_->setText (QString::number (info.dirsCount_));
    ui_->sizeLabel_->setText (humanSize (info.size_));
    ui_->averageSizeLabel_->setText (info.filesCount_ == 0 ? "0" : humanSize (info.size_ / info.filesCount_));

    int row = 0;
    for (const auto &[fileType, countAndSize] : info.typesSize_) {
        const auto averageSize = countAndSize.second == 0 ? 0 : countAndSize.second / countAndSize.first;
        const QString text = QString ("%1 - %2").arg (fileType).arg (humanSize (averageSize));
        if (ui_->fileTypeSizesList_->count () <= row) {
            ui_->fileTypeSizesList_->addItem (text);
        } else {
            ui_->fileTypeSizesList_->item (row)->setText (text);
        }
        ++row;
    }
    while (row != ui_->fileTypeSizesList_->count ()) {
        delete ui_->fileTypeSizesList_->takeItem (row);
    }
}

QString MainWindow::humanSize (quint64 bytes) const
{
    constexpr quint64 kb = 1024;
    constexpr quint64 mb = 1024 * kb;
    constexpr quint64 gb = 1024 * mb;
    constexpr quint64 tb = 1024 * gb;

    if (bytes >= tb) {
        return tr ("%1 TB").arg (static_cast<double> (bytes) / tb, 0, 'f', 3);
    }
    if (bytes >= gb) {
        return tr ("%1 GB").arg (static_cast<double> (bytes) / gb, 0, 'f', 2);
    }
    if (bytes >= mb) {
        return tr ("%1 MB").arg (static_cast<double> (bytes) / mb, 0, 'f', 1);
    }
    if (bytes >= kb) {
        return tr ("%1 KB").arg (static_cast<double> (bytes) / kb, 0, 'f', 1);
    }
    return tr ("%1 bytes").arg (bytes);
}

void MainWindow::updateCurrentProcessesInfo ()
{
    ui_->currentProcessesList_->clear ();
    for (DirInfo *dirInfo : dirInfoList_) {
        if (!dirInfo->isFinished ()) {
            ui_->currentProcessesList_->addItem (QString ("Scan dir %1").arg (dirInfo->path ()));
        }
    }
}
