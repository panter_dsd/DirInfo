Необходимо разработать приложение под windows, которое предоставляет статистику использования файловой системы.

Есть дерево каталогов выбранного диска. При выборе папки показывается статистика по ней:
- количество файлов, общий размер и средний размер для каждой группы файлов (группировать по расширению и отдельной
строкой - для всех) - рекурсивно по всей папке.
- количество подкаталогов - только для данной папки.
Интерфейс пользователя должен отображать статус всех асинхронных операций - построение дерева каталогов, подсчет
статистки по каталогу.

Подсчёт статистики должен производиться в фоновом потоке, интерфейс пользователя не должен замирать или блокироваться.
Писать на С++/Qt, любых версий. Использование средств C++ 11 приветствуется.

Максимальная простота, понятность и минимальное количество кода.

В архиве нужны как исходники, так и исполнимый файл со всеми зависимостями (или статически слинкованный), чтобы
можно было запустить на чистой машине.

При выполнении задания обратите внимание на то, что мы будем оценивать не только работоспособность программы,
но и аккуратность кода, наличие комментариев и т.п.. Однако, мы не ждем сложной универсальной иерархии классов.
Делайте решение и демо приложение максимально компактным и простым.
