#pragma once

#include <QtCore/QList>
#include <QtCore/QMetaObject>

#include <QtWidgets/QMainWindow>

#include <memory>

class DirInfo;

using DirInfoList = QList<DirInfo *>;

class MainWindow : public QMainWindow
{
Q_OBJECT
public:
    MainWindow ();

    virtual ~MainWindow ();

private:
    Q_DISABLE_COPY (MainWindow)

    void onDriveSelect (int index);

    void onFolderSelect (const QModelIndex &index);

    QString humanSize (quint64 bytes) const;

    void updateInfoPanel (DirInfo *dirInfo = nullptr);

    void changeCurrentDirInfo (DirInfo *dirInfo);

    void updateCurrentProcessesInfo ();

private:
    std::unique_ptr<class Ui_MainWindow> ui_;

    class QFileSystemModel *dirTreeModel_;

    DirInfoList dirInfoList_;
    DirInfo *currentDirInfo_;
    QMetaObject::Connection currentDirInfoConnection_;
};

